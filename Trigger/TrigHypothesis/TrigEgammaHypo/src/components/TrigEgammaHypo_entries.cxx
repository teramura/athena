#include "../TrigEgammaFastPhotonFexMT.h"
#include "../TrigEgammaFastCaloHypoAlgMT.h"
#include "../TrigEgammaFastCaloHypoToolInc.h"
#include "../TrigEgammaPrecisionCaloHypoToolInc.h"
#include "../TrigEgammaPrecisionEtcutHypoTool.h"
#include "../TrigEgammaPrecisionPhotonHypoToolInc.h"
#include "../TrigEgammaPrecisionElectronHypoToolInc.h"
#include "../TrigEgammaFastElectronHypoTool.h"
#include "../TrigEgammaFastElectronFexMT.h"
#include "../TrigEgammaFastElectronHypoAlgMT.h"
#include "../TrigEgammaFastPhotonHypoAlgMT.h"
#include "../TrigEgammaFastPhotonHypoTool.h"
#include "../TrigEgammaPrecisionCaloHypoAlgMT.h"
#include "../TrigEgammaPrecisionEtcutHypoAlgMT.h"
#include "../TrigEgammaPrecisionPhotonHypoAlgMT.h"
#include "../TrigEgammaPrecisionElectronHypoAlgMT.h"
#include "../TrigEgammaMassHypoTool.h"
#include "../TrigEgammaDPhiHypoTool.h"

DECLARE_COMPONENT( TrigEgammaFastPhotonFexMT )
DECLARE_COMPONENT( TrigEgammaFastCaloHypoAlgMT )
DECLARE_COMPONENT( TrigEgammaFastElectronHypoAlgMT )
DECLARE_COMPONENT( TrigEgammaFastPhotonHypoAlgMT )
DECLARE_COMPONENT( TrigEgammaFastCaloHypoToolInc )
DECLARE_COMPONENT( TrigEgammaPrecisionCaloHypoToolInc )
DECLARE_COMPONENT( TrigEgammaPrecisionEtcutHypoTool )
DECLARE_COMPONENT( TrigEgammaPrecisionPhotonHypoToolInc )
DECLARE_COMPONENT( TrigEgammaPrecisionElectronHypoToolInc )
DECLARE_COMPONENT( TrigEgammaFastElectronHypoTool )
DECLARE_COMPONENT( TrigEgammaFastElectronFexMT )
DECLARE_COMPONENT( TrigEgammaFastPhotonHypoTool )
DECLARE_COMPONENT( TrigEgammaPrecisionCaloHypoAlgMT )
DECLARE_COMPONENT( TrigEgammaPrecisionEtcutHypoAlgMT )
DECLARE_COMPONENT( TrigEgammaPrecisionPhotonHypoAlgMT )
DECLARE_COMPONENT( TrigEgammaPrecisionElectronHypoAlgMT )
DECLARE_COMPONENT( TrigEgammaMassHypoTool )
DECLARE_COMPONENT( TrigEgammaDPhiHypoTool )
